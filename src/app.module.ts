import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostsModule } from './posts/posts.module';
import { CommentsModule } from './comments/comments.module';
import { PostsService } from './posts/posts.service';
import { CommentsService } from './comments/comments.service';
import { PostsController } from './posts/posts.controller';
import { CommentsController } from './comments/comments.controller';
import { DataStorageService } from './shared/dataStorage.service';
import { ApiService } from './shared/api.service';

@Module({
  imports: [
    HttpModule,
    PostsModule, 
    CommentsModule
  ],
  controllers: [
    AppController,
    PostsController,
    CommentsController
  ],
  providers: [
    AppService,
    DataStorageService,
    ApiService,
    PostsService,
    CommentsService
  ],
})
export class AppModule {}
