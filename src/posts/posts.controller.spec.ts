import { Test } from '@nestjs/testing';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import { CommentsService } from '../comments/comments.service';
import { CommentDto } from '../comments/dto/comment.dto';
import { PostDto } from './dto/post.dto';
import { DataStorageService } from '../shared/dataStorage.service';
import { HttpModule } from '@nestjs/common';

describe('PostsController', () => {
  let postsController: PostsController;
  let postsService: PostsService;
  let commentsService: CommentsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        imports: [HttpModule],
        controllers: [PostsController],
        providers: [PostsService, CommentsService, DataStorageService],
      }).compile();

    postsService = moduleRef.get<PostsService>(PostsService);
    commentsService = moduleRef.get<CommentsService>(CommentsService);
    postsController = moduleRef.get<PostsController>(PostsController);
  });

  describe('findAll', () => {
    it('should return an array of PostDto', async () => {
      const commentResult: CommentDto[] = [{
        postId: 1,
        id: 1,
        name: "id labore ex et quam laborum",
        email: "Eliseo@gardner.biz",
        body: "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
      }];
      const postResult: PostDto[] = [{
        postId: 1,
        postTitle: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        postBody: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
        comments: commentResult,
        totalNumberOfComments: 1
        }];
      jest.spyOn(postsService, 'findAll').mockImplementation(() => Promise.resolve(postResult));
      jest.spyOn(commentsService, 'findAll').mockImplementation(() => Promise.resolve(commentResult));
      expect(await postsController.findAll()).toMatchObject(postResult);
    });
  });
});