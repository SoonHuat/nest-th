import { Controller, Get, Param } from '@nestjs/common';
import * as _ from 'lodash';
import { forkJoin } from 'rxjs';
import { PostDto } from './dto/post.dto';
import { PostsService } from './posts.service';
import { CommentsService } from '../comments/comments.service';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService,
    private readonly commentsService: CommentsService) {}

  @Get()
  async findAll(): Promise<PostDto[]> {
    let posts: PostDto[] = [];
    const result = await forkJoin(this.postsService.findAll(),
      this.commentsService.findAll()).toPromise();
      
    posts = result[0];
    const comments = result[1];
    _.each(posts, post => {
      const postComments = _.filter(comments, comment => {
        return comment.postId == post.id;
      });
      post.comments = postComments;
      post.totalNumberOfComments = postComments.length;
    });
    posts = _.sortBy(posts, ['totalNumberOfComments']);
    return posts;
  }

  @Get(':id')
  async find(@Param('id') id): Promise<PostDto> {
    const post = this.postsService.find(id);
    return post;
  }
}