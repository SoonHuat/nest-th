import { Module, HttpModule } from '@nestjs/common';
import { DataStorageService } from 'src/shared/dataStorage.service';
import { ApiService } from 'src/shared/api.service';

@Module({
  imports: [
    HttpModule
  ],
  providers: [
    DataStorageService,
    ApiService
  ]
})
export class CommentsModule {}
