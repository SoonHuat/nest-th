import { Injectable } from '@nestjs/common';
import { CommentDto } from './dto/comment.dto';
import { DataStorageService } from '../shared/dataStorage.service';
import { ApiService } from 'src/shared/api.service';

@Injectable()
export class CommentsService {

  constructor(private readonly apiService: ApiService,
    private readonly dataStorageService: DataStorageService) {}
  
  async findAll(): Promise<CommentDto[]> {
    const baseUrl = "https://jsonplaceholder.typicode.com/";
    const token = "comments";
    const cachedComments = this.dataStorageService.get(token);
    if (cachedComments) {
      return cachedComments as CommentDto[];
    }
    const url = baseUrl + token;
    const comments = await this.apiService.get(url);
    return comments;
  }
}