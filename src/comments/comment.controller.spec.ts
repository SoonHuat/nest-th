import { Test } from '@nestjs/testing';
import { CommentsService } from '../comments/comments.service';
import { CommentDto } from '../comments/dto/comment.dto';
import { DataStorageService } from '../shared/dataStorage.service';
import {  HttpModule } from '@nestjs/common';
import { CommentsController } from './comments.controller';

describe('CommentsController', () => {
  let commentsController: CommentsController;
  let commentsService: CommentsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
        imports: [HttpModule],
        controllers: [CommentsController],
        providers: [CommentsService, DataStorageService],
      }).compile();

    commentsService = moduleRef.get<CommentsService>(CommentsService);
    commentsController = moduleRef.get<CommentsController>(CommentsController);
  });

  describe('findAll', () => {
    it('should return an array of CommentDto', async () => {
      const commentResult: CommentDto[] = [{
        postId: 1,
        id: 1,
        name: "id labore ex et quam laborum",
        email: "Eliseo@gardner.biz",
        body: "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium"
      }];
      jest.spyOn(commentsService, 'findAll').mockImplementation(() => Promise.resolve(commentResult));
      expect(await commentsController.findAll(null)).toMatchObject(commentResult);
    });
  });
});